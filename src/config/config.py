import yaml

config_dico = {}


def load_config():
    global config_dico
    yaml_file = open("config/config.yaml", "r")

    yaml_content = yaml.load(yaml_file, Loader=yaml.FullLoader)
    config_dico = {}
    for key, value in yaml_content.items():
        config_dico[key] = value

    yaml_file.close()


def get(key_looked):
    global config_dico
    return config_dico[key_looked]
