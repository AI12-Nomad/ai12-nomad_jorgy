from src.client.data.data_controller import DataController
from src.common.data_structures.profiles import Player
from common import PublicGame
from typing import List


class CommCallsDataImpl:
    def store_connected_user(self, players: List):
        for player in players:
            if player not in DataController.connected_players:
                DataController.connected_players.add(player)

    def store_games(self, games: List):
        for game in games:
            if game not in DataController.available_games:
                DataController.available_games.add(game)

    def add_connected_user(self, player: Player):
        if player not in DataController.players:
            DataController.players.append(player)

    def store_new_game(self, public_game: PublicGame):
        if public_game not in DataController.public_game:
            DataController.public_game.append(public_game)

    def player_join_game(self, player: Player):
        if DataController.local_game != None:
            if DataController.local_game.red_player == None:
                DataController.local_game.red_player = player
            elif DataController.local_game.black_player == None:
                DataController.local_game.black_player = player

    def update_public_game(self, public_game: PublicGame):
        DataController.public_game = public_game
