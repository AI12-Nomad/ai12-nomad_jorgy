import typing
from typing import List

import pygame_gui

# it's a way to avoid circular imports (do not use from ... import ...)
# import client.ihm.common.ui_renderer as UIRenderer
from client.ihm.common.component import Component

# from client.ihm.main.ihm_main_controller import IHMMainController


class View:
    def __init__(
        self, pygame_manager: pygame_gui.UIManager, ui, controller: typing.Any
    ):
        self.pygame_manager = pygame_manager
        self.components: List[Component] = []
        self.ui = ui
        self.controller = controller

    def render(self):
        for component in self.components:
            component.render()

    def handle_event(self, event):
        for component in self.components:
            component.handle_event(event)

    def add(self, component: Component):
        self.components.append(component)
        component
