from typing import List
from common import PublicGame, Player


class DataServerController:
    game_list: List[PublicGame] = []
    player_list: List[Player] = []
