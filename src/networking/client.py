import asyncio
from .common import IO


async def create_client(ip, port):
    loop = asyncio.get_running_loop()
    on_connection_lost = loop.create_future()
    io = IO(on_connection_lost)
    _, _ = await loop.create_connection(io, ip, port)
    return io
